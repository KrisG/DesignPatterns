﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton
{
    class Program
    {
        public class Singleton
        {
            private static Singleton _instance;

            private Singleton(){ }

            public static Singleton Instance
            {
                get
                {
                    if (_instance == null)
                    {
                        _instance = new Singleton();
                        Console.WriteLine("Instance created!");
                    }
                    return _instance;
                }
            }

            public void Test()
            {
                Console.WriteLine("Hello from singleton!");
            }

            public void Test2()
            {
                Console.WriteLine("Hello from same instance!");
                Console.ReadLine();
            }
        }

        static void Main(string[] args)
        {
            Singleton.Instance.Test();
            Singleton.Instance.Test2();
        }
    }
}
