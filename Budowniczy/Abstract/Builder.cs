﻿using Budowniczy.Model;

namespace Budowniczy.Abstract
{
    public abstract class Builder : IBuilder
    {
        public abstract void CreateBurger();
        public abstract void CreateFries();
        public abstract void CreateSoda();
        public abstract void CreateDessert();
        public abstract Meal GetMeal();
    }
}