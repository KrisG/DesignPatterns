﻿using Budowniczy.Model;

namespace Budowniczy.Abstract
{
    public interface IBuilder
    {
        void CreateBurger();
        void CreateFries();
        void CreateSoda();
        void CreateDessert();
        Meal GetMeal();
    }
}