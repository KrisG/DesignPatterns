﻿using Budowniczy.Abstract;
using Budowniczy.Model;

namespace Budowniczy.Concrete
{
    public class HappyMealBuilder : Builder
    {
        private Meal _meal = new Meal();

        public override void CreateBurger()
        {
            _meal.Burger = "Hamburger";
        }

        public override void CreateFries()
        {
            _meal.Fries = "CurlyFries";
        }

        public override void CreateSoda()
        {
            _meal.Soda = "Cola";
        }

        public override void CreateDessert()
        {
            _meal.Dessert = "Ice cream";
        }

        public override Meal GetMeal()
        {
            return _meal;
        }
    }
}