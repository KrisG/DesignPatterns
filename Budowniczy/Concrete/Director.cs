﻿using Budowniczy.Abstract;
using Budowniczy.Model;

namespace Budowniczy.Concrete
{
    public class Director
    {
        public void Construct(IBuilder builder)
        {
            builder.CreateBurger();
            builder.CreateDessert();
            builder.CreateFries();
            builder.CreateSoda();
        }
    }
}