﻿using Budowniczy.Abstract;
using Budowniczy.Model;

namespace Budowniczy.Concrete
{
    public class BigSetBuilder : Builder
    {
        private Meal _meal = new Meal();

        public override void CreateBurger()
        {
            _meal.Burger = "BIG burger";
        }

        public override void CreateFries()
        {
            _meal.Fries = "BIG fries";
        }

        public override void CreateSoda()
        {
            _meal.Soda = "BIG Cola";
        }

        public override void CreateDessert()
        {
            _meal.Dessert = "BIG Ice cream";
        }

        public override Meal GetMeal()
        {
            return _meal;
        }
    }
}