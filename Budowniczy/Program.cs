﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Budowniczy.Abstract;
using Budowniczy.Concrete;
using Budowniczy.Model;

namespace Budowniczy
{
    class Program
    {
        public static void ReadMeal(Meal meal)
        {
            Console.WriteLine(meal.Burger);
            Console.WriteLine(meal.Fries);
            Console.WriteLine(meal.Soda);
            Console.WriteLine(meal.Dessert);
        }

        static void Main(string[] args)
        {
            IBuilder smallMealBuilder = new HappyMealBuilder();
            IBuilder bigMealBuilder = new BigSetBuilder();

            var director = new Director();

            Console.WriteLine("Creating happy meal:");
            director.Construct(smallMealBuilder);
            ReadMeal(smallMealBuilder.GetMeal());
            

            Console.WriteLine("Creating BIG meal:");
            director.Construct(bigMealBuilder);
            ReadMeal(bigMealBuilder.GetMeal());

            Console.ReadLine();
        }
    }
}
