﻿namespace Budowniczy.Model
{
    public class Meal
    {
        public string Burger { get; set; }
        public string Soda { get; set; }
        public string Fries { get; set; }
        public string Dessert { get; set; }
    }
}