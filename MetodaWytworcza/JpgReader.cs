﻿using MetodaWytworcza.Core;

namespace MetodaWytworcza
{
    public class JpgReader : IImageReader
    {
        private string Image { get; set; }

        public JpgReader(string image)
        {
            this.Image = image;
        }

        public string GetDecodedImage()
        {
            return $"Your .JPG {this.Image}";
        }
    }
}