﻿using MetodaWytworcza.Core;

namespace MetodaWytworcza
{
    public class PngReader : IImageReader
    {
        private string Image { get; set; }

        public PngReader(string image)
        {
            this.Image = image;
        }

        public string GetDecodedImage()
        {
            return $"Your .PNG: {this.Image}";
        }
    }
}