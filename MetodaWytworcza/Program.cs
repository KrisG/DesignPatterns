﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetodaWytworcza.Core;

namespace MetodaWytworcza
{
    class Program
    {
        static void Main(string[] args)
        {
            IImageReader reader = null;

            var extension = Console.ReadLine();

            if (extension == "jpg")
            {
                reader = new JpgReader("cat");
            }
            else
            {
                reader = new PngReader("dog");
            }

            Console.WriteLine(reader.GetDecodedImage());
            Console.ReadLine();
        }
    }
}
