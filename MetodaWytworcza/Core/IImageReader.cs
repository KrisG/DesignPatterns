﻿namespace MetodaWytworcza.Core
{
    public interface IImageReader
    {
        string GetDecodedImage();
    }
}