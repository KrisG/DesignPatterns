﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class SportEngine : IEngine
    {
        public string Name { get; set; } = "Sports car engine";
    }
}