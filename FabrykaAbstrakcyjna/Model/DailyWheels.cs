﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class DailyWheels : IWheels
    {
        public string Name { get; set; } = "Daily car wheels";
    }
}