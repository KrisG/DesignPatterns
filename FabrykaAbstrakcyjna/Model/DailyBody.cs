﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class DailyBody : IBody
    {
        public string Name { get; set; } = "Daily car body";
    }
}