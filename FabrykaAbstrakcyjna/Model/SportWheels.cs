﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class SportWheels : IWheels
    {
        public string Name { get; set; } = "Sports car wheels";
    }
}