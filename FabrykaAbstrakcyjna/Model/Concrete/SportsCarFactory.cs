﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model.Concrete
{
    public class SportsCarFactory : IFactory
    {
        public IWheels CreateWheels()
        {
            return new SportWheels();
        }

        public IBody CreateBody()
        {
            return new SportBody();
        }

        public IEngine CreateEngine()
        {
            return new SportEngine();
        }
    }
}