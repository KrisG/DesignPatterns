﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model.Concrete
{
    public class DailyCarFactory : IFactory
    {
        public IWheels CreateWheels()
        {
            return new DailyWheels();
        }

        public IBody CreateBody()
        {
            return new DailyBody();
        }

        public IEngine CreateEngine()
        {
            return new DailyEngine();
        }
    }
}