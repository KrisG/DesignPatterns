﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class SportBody : IBody
    {
        public string Name { get; set; } = "Sports car body";
    }
}