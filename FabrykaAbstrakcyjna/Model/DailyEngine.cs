﻿using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna.Model
{
    public class DailyEngine : IEngine
    {
        public string Name { get; set; } = "Daily car engine";
    }
}