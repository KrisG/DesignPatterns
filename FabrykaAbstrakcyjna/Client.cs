﻿using System;
using FabrykaAbstrakcyjna.Core.Abstract;

namespace FabrykaAbstrakcyjna
{
    public class Client
    {
        private readonly IWheels _wheels;
        private readonly IBody _body;
        private readonly IEngine _engine;

        public Client(IFactory factory)
        {
            _body = factory.CreateBody();
            _engine = factory.CreateEngine();
            _wheels = factory.CreateWheels();
        }

        public void GetMyCar()
        {
            Console.WriteLine("Creating new car...");
            Console.WriteLine("This car is made of:");
            Console.WriteLine(_body.Name);
            Console.WriteLine(_engine.Name);
            Console.WriteLine(_wheels.Name);
        }
    }
}