﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FabrykaAbstrakcyjna.Core.Abstract;
using FabrykaAbstrakcyjna.Model.Concrete;

namespace FabrykaAbstrakcyjna
{
    class Program
    {
        static void Main(string[] args)
        {
            IFactory sportsCarFactory = new SportsCarFactory();
            IFactory dailyCarFactory = new DailyCarFactory();

            var sportsClient = new Client(sportsCarFactory);
            var dailyClient = new Client(dailyCarFactory);
            
            sportsClient.GetMyCar();
            dailyClient.GetMyCar();

            Console.ReadLine();
        }
    }
}
