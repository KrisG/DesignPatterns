﻿namespace FabrykaAbstrakcyjna.Core.Abstract
{
    public interface IFactory
    {
        IWheels CreateWheels();
        IBody CreateBody();
        IEngine CreateEngine();
    }
}