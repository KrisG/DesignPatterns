﻿namespace FabrykaAbstrakcyjna.Core.Abstract
{
    public interface IEngine
    {
        string Name { get; set; }
    }
}