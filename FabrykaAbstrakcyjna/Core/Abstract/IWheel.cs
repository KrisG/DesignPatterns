﻿namespace FabrykaAbstrakcyjna.Core.Abstract
{
    public interface IWheels
    {
        string Name { get; set; }
    }
}