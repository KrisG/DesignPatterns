﻿namespace FabrykaAbstrakcyjna.Core.Abstract
{
    public interface IBody
    {
        string Name { get; set; }
    }
}